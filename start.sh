#!/bin/bash
WEBSITE_FOLDER="./samplephpwebsite/"
DOCKER_FOLDER=".."
VERSION=1
PID=$!

git clone https://github.com/Akasam/samplephpwebsite.git

pause() {
    read -p "Press any key to check next version..."
}

changeVersion() {
    docker-compose down
    echo "Checkout to ${VERSION}..."
    git checkout v${VERSION}
    echo "Enter to samplephpwebsite folder..."
    cd "${WEBSITE_FOLDER}"
    echo "Change website version..."
    git checkout v${VERSION}
    cd "${DOCKER_FOLDER}"
    echo "Version ${VERSION} is now deployed !"
    echo ""
    echo ""
    echo ""
    docker-compose up
    pause
}

changeVersion ${VERSION}
VERSION=2
changeVersion ${VERSION}
VERSION=3
changeVersion ${VERSION}