# Sample php website documentation

Identifiant Jenkins : 
    Login   : romain
    Mdp     : root

Ceci est une documentation visant à expliquer comment tester les trois versions de ce site vie un script.sh.

## Lancement et test du projet

**Executer** le script `start.sh` afin de réaliser les tests.

`$ chmod +x start.sh`

`$ ./start.sh`

Les versions seront testées une par une en commençant par la première. Lorsque vous voudrez changer de version, aller dans votre terminal et faite `CRTL+C` afin de stopper le docker-compose. Appuyez ensuite sur n'importe quelle touche pour passer à la version suivante.

## V1

* Création d'un `docker-compose.yml` avec un container nginx et un php
* Versions des images :
  _ `nginx:alpine`
  _ `php:fpm-alpine`
* Nginx écoute sur le port 80 et php sur le port 9000.

* Contennu du dossier `samplephpwebsite` copié dans les containers.

## V2

* `$ git merge v1`
* Modification des versions des images :
  _ `nnginx:1.12-alpine`
  _ `php:7.1fpm-alpine`

## V3

* `$ git merge v1`
* Ajout d'un container db avec mariadb
* Ajout d'une image: \* `mariadb:10.1`
* Création d'un `Dockerfile` pour installer des modules php :
  _ mysqli
  _ gd
* Utilisation du `Dockerfile` dans le `docker-compose.yml`
* Liaison du container db au container php
